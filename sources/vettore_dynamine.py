#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  vettore_dynamine.py
#  
#  Copyright 2017  <scimmia@mira>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from vector_builder.vettore_gen import build_vector
features='dyna_back'## per le altre features: dyna_sheet, dyna_coil, dyna_helix, ef, dyna_side
sequenza='AAAAAAAAAAAAAAAAAAAAAAAAAAACCCCCCCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAAAAACCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAA'
lista=build_vector(sequenza,features,0)#
predizione= [item for sublist in lista for item in sublist]
print predizione
