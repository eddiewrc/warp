#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  warpUtils.py
#  
#  Copyright 2017 Daniele Raimondi <daniele.raimondi@vub.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

amino_acid_codes = [  "A",  "C",  "D",  "E",  "F",  "G",  "H",  "I",  "K",  "L",  "M",  "N",  "P",  "Q",  "R",  "S",  "T",  "V",  "W",  "Y"  ]

def readInput(f):
	ifp = open(f)
	db = []
	line = ifp.readline()
	i = 0
	discarded = 0
	while len(line) > 0:
		tmp = line.split()
		if len(tmp) != 2:
			print "ERROR on line %d: expected two tab separated sequences, found this: %s" % (i, str(tmp))
			return 1
			discarded += 1
		else:
			db.append((tmp[0],tmp[1]))
		i += 1
		line = ifp.readline()
	print "Found %d pairs, discarded %d" % (i, discarded)
	return db



def getOrthoVect(seq):
	aleph = ["A","R", "N","D","C","Q","E","G","H","I","L","K","M","F","P","S","T","W","Y","V"] 
	r = []
	for s in seq: 	
		tmp = [0]*20
		try:
			tmp[aleph.index(s)] = 1	
		except:
			pass
		r.append(tmp)	
	#for i in r:
	#	assert len(i) == 20		
	return r

def getFreqs(seq):
	f = []
	for aa in amino_acid_codes:
		f.append(seq.count(aa)/float(len(seq)))
	assert max(f) < 1
	assert len(f) == 20
	return f

def printHelp():	
	print "\nUSAGE: python WARP.py INPUT_FILE"
	print "\nINPUT_FILE contains pairs of protein sequences, one pair per line, separate by tabs. WARP will output one [0,1] homology prediction for each pair in the output file.\n"

def main():
	return 0

if __name__ == '__main__':
	main()
