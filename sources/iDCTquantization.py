#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it, daniele.raimondi@vub.ac.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import numpy as np
from scipy.fftpack import fft, ifft
from scipy.fftpack import dct, idct
epsilon = 0.00001

def iDCTquantization(v, PLOT = False, LEN = 10): #mean	
	v = np.array(v)		
	if v.ndim > 1:
		v = np.array(v).T
		assert np.max(v) <= 1 and np.min(v) >= 0		
		yf = dct(v, type=2, norm="ortho")		
		trans = idct(yf[:,:LEN], type=2, norm="ortho")			
		i = 0
		tmp = []
		while i < len(trans):
			if not np.any(v[i]):			
				tmp.append(trans[i])
			else:
				tmp.append(scale(trans[i]))	
			i+=1		
		assert len(tmp) == len(v)
		i = 0
		while i < len(tmp):
			if len(tmp) < LEN:
				tmp[i] = tmp[i].tolist() + [0]*(LEN-len(tmp[i]))
				assert len(tmp[i]) == LEN
			i += 1		
		tmp = np.array(tmp)
		return tmp.T 	
	else:
		v = scale(np.array(v))	
		assert max(v) <= 1 and min(v) >= 0		
		yf = dct(v, type=2, norm="ortho")
		trans = idct(yf[:LEN], type=2, norm="ortho")
		tmp = scale(trans)
		if len(tmp) < LEN:
			tmp = list(tmp) + [0]*(LEN-len(tmp))
		assert len(tmp) == LEN
		tmp = np.array(tmp)
		return tmp	

def scale(v, a = 0, b = 1):
	if not np.any(v):
		return v
	M = np.max(v)
	m = np.min(v)	
	return (v - m) / float(M - m)		

def readDynaFormat(f, PRINT = False):
	db = {}
	print "Reading ",f
	ifp = open(f)
	lines = ifp.readlines()
	ifp.close()
	tmp = []
	name = ""
	
	if not "*" in lines[0]:
		l = 1
	else:
		l = 0
	while l < len(lines):
		if lines[l][0] == "*":
			name = lines[l+4].strip()
			#name = lines[l+4][lines[l+4].index("_")+1:lines[l+4].index("_",10)].strip()
			#print name
			#raw_input()
			#print name
			l+=11
		assert lines[l] != "*"
		while l < len(lines) and lines[l][0] != "*":
			if lines[l][0] == " ":
				l+=1
				continue
			#print lines[l][0]
			tmp.append(float(lines[l][3:].strip()))			
			l+=1		
		db[name] = tmp
		#db[name.replace("_",".")] = tmp
		tmp = []
		
		assert "" not in db.keys()		
	
	return db	

def main1():
	DATASET_BENCHMARK = "supfam"
	from utils import readFASTA, convertSeq
	seqdb = readFASTA("../Homology_Benchmark/fastas/homologs_"+DATASET_BENCHMARK+".fasta", MIN_LENGTH = 0, MAX_LENGTH=500000) # {id:seq}
	s = seqdb.items()[0]
	#print s
	#iDCTquantizationSKscaled(convertSeq(s[1], 1),PLOT = False, LEN = 1)
	iDCTquantization(convertSeq(s[1], 1),PLOT = False, LEN = 10)
	iDCTquantization(convertSeq(s[1], 1),PLOT = False, LEN = 10)
	main1()

def main():
	backbone = readDynaFormat("test")
	v = backbone.items()[0]
	#print v[1]
	print iDCTquantization([[0,1],[0,0],[1,0]], PLOT = False, LEN = 10)
	print iDCTquantization([1,2,3,4,5,6,7,8], PLOT = False, LEN = 8), scale(np.array([1,2,3,4,5,6,7,8]))
	print iDCTquantization([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], PLOT = False, LEN = 5)
	#iDCTquantization1(v[1], PLOT = True, LEN = 10)
	#iDCTquantization1(v[1], PLOT = True, LEN = 10)
	#print np.mean(v[1]), np.mean(iDCTquantizationScaled(v[1], LEN = 1, PLOT=False))
	#FFTquantization1([v[1]], PLOT=True, LEN=50)
	#FFTquantization([v[1]], PLOT=True)
if __name__ == '__main__':
	main()
