#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  WARP!.py
#  
#  Copyright 2017 Daniele Raimondi <daniele.raimondi@vub.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import warnings
warnings.filterwarnings("ignore") #not elegant but will fix in the future plus we know exactly the reason, so nothing to worry about
import sys, os, cPickle
from sources import warpUtils as U
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics.pairwise import chi2_kernel
from sources.iDCTquantization import iDCTquantization
from fastdtw.fastdtw import fastdtw
from vector_builder.vettore_gen import build_vector
LEN_FFT = 50

'''
This is a scripted code and its purpose is just to provide a sketchy proof of concept about how WARP works, in relation to the methods
explained in the article. We will provide soon a comprehensive and *really* usable implementation of WARP with the goal of substantially speeding up
the time required for homology detection in the daily life of structural bioinformaticians. This task will awnyway require a less theoretical approach and a 
specific infrastructure that at the moment we cannot provide.

The code here contains anyway ALL the concepts described in the paper (such as the iDCTquantization) and you are free to use them and hack it as you wish.
'''

def main(args):
	print "WARP 1.0 - Alignment free homology detection tool\nPlease cite Raimondi et al. XXX (under review)\n\n"
	if len(args) != 2 or "-h" in args[1]:
		U.printHelp()
		return 1
	print "Reading inputs..."
	inputPairs = U.readInput(args[1])
	print "Computing features..."
	db = []
	for p in inputPairs:
		x = [U.getFreqs(p[0]), iDCTquantization(U.getOrthoVect(p[0]),LEN = LEN_FFT),\
		iDCTquantization(getDynaminePreds(p[0], "dyna_back"),PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(getDynaminePreds(p[0], "dyna_side"),PLOT = False, LEN = LEN_FFT)]
		y = [U.getFreqs(p[1]), iDCTquantization(U.getOrthoVect(p[1]),LEN = LEN_FFT),\
		iDCTquantization(getDynaminePreds(p[1], "dyna_back"),PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(getDynaminePreds(p[1], "dyna_side"),PLOT = False, LEN = LEN_FFT)]
		assert len(x) == len(y)		
		db.append((x,y))
	print "Computing DTW distances..."
	assert len(db) == len(inputPairs)
	i = 0
	x = []
	for p in db:
		a = p[0]
		b = p[1]
		d = []			
		assert len(a) == len(b)
		t = 1
		d.append(chi2_kernel([a[0]],[b[0]])[0][0])
		while t < len(a):						
			d.append(fastdtw(a[t], b[t], dist=None)[0])			
			t += 1			
		x.append(d)	
		i += 1
	assert len(x)
	print "Loading trained model..."
	model = cPickle.load(open("trainedModels/warp.RF.pfam.model.noPSIPRED.cPickle"))
	print "Predicting..."	
	yp = model.predict_proba(x)[:,1]
	outfile = args[1]+".warp.out"
	print "Writing results in %s " % outfile
	ofp = open(outfile, "w")
	ofp.write("#WARP predictions (>0.5 means HOMOLOGY)\n")
	for i in yp:
		ofp.write("%f\n"%i)
	ofp.close()
	return 0

def getDynaminePreds(seq, feat):
	lista = build_vector(seq, feat,0)#	
	predizione = [item for sublist in lista for item in sublist]
	return predizione

if __name__ == '__main__':    
	sys.exit(main(sys.argv))
