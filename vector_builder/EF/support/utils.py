#!/usr/bin/env python
# encoding: utf-8
"""
utils.py

Created by Elisa Cilia on 2014-08-14.
Copyright (c) 2014 Elisa Cilia. All rights reserved.
"""

import numpy

import config


def correctFragment(frag):
    seq = ''
    for j in frag:
        if not j[0] in config.aas:
            seq += 'X'
        else:
            seq += j[0]
    return seq


##################
### Stat utils ###
##################

def median(values):
    return numpy.median(values)

def filterOutliers(values):
    q3 = numpy.percentile(values, 75, interpolation='higher')
    q1 = numpy.percentile(values, 25, interpolation='lower')
    iqr = q3 - q1
    newdistribution = [v for v in values if v >= q1 - 1.5 * iqr and v <= q3 + 1.5 * iqr]
    return newdistribution
