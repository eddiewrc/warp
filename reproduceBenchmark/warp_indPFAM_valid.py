#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  homology1.py
#  
#  Copyright 2017 Daniele Raimondi <eddiewrc@arcturus>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import matplotlib.pyplot as plt
from fastdtw._fastdtw import fastdtw
from fastdtw._fastdtw import dtw
from sources.parseBenchmark import parseBench
from sources.parsePSIPRED4 import parsePSIPRED
import sources.utils as U
import numpy as np
import random, cPickle
from scipy.stats import ranksums
from sklearn.linear_model import LogisticRegression, RidgeClassifier
from sklearn.svm import SVC		
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics.pairwise import chi2_kernel
from sources.iDCTquantization import iDCTquantization
RANDOM = False
FAST = True
PROBABILITY = True
LEN_FFT = 50
STORE_PREDS = False
DATASET_BENCHMARK = "pfam"
SAVEFIG=False
DIST = None
FREQS = True
if RANDOM:
	FREQS = False
OOB = False
# > Best threshold: 0.431671132381
def main():
	random.seed(100)
	print DATASET_BENCHMARK, LEN_FFT,STORE_PREDS, FAST, " OOB: ", OOB
	db, uids = parseBench("Homology_Benchmark/Methods_benchmarking_pairs/"+DATASET_BENCHMARK+"_hom-pairs_max50.txt","Homology_Benchmark/Methods_benchmarking_pairs/"+DATASET_BENCHMARK+"_nonhom-pairs_max50.txt")
	seqdb = U.readFASTA("Homology_Benchmark/fastas/homologs_"+DATASET_BENCHMARK+".fasta", MIN_LENGTH = 0, MAX_LENGTH=500000) # {id:seq}
	ssdb = parsePSIPRED("data/psipred4_"+DATASET_BENCHMARK+"/")
	backbone = U.readDynaFormat("data/dynaMineResults_"+DATASET_BENCHMARK+"/homologs_backbone.pred")	
	side = U.readDynaFormat("data/dynaMineResults_"+DATASET_BENCHMARK+"/homologs_sidechain.pred")
	vdb = vectorizeDCTSeqs(seqdb, ssdb, backbone, side)
	cv_pairs = U.randomizePairsPerID(db)#train, test
	#cPickle.dump(cv_pairs, open("separatedPairs.cPickle","w"))

	totyp = []
	toty = []
	
	print "Split train and test"
	testIds = cv_pairs[1]
	trainIds = cv_pairs[0]
	#assert len(testIds) + len(trainIds) == len(db)	
	print "Preparing vectors..."
	X = []
	Y = []
	i = 0	
	for p in trainIds[:]:
		if i % 1000 == 0:
			print i, len(trainIds)			
		try:
			a = vdb[p[0]]
			b = vdb[p[1]]
		except:
			continue			
		d = []
		assert len(a) == len(b)
		if FREQS:
			t = 1
			d.append(chi2_kernel([a[0]],[b[0]])[0][0])
		else:
			t = 0
		while t < len(a):	
			if FAST:							
				d.append(fastdtw(a[t], b[t], dist=DIST)[0])
			else:
				d.append(dtw(a[t], b[t], dist=DIST)[0])
			t += 1			
		X.append(d)			
		Y.append(db[p])
		i+=1
	assert len(X) == len(Y)
	print "Training..."
	print "Len vectors: ", len(X[0])	
	#model = SVC(C=1.0, kernel='rbf', degree=3, gamma="auto", coef0=0.0, shrinking=True, probability=PROBABILITY, tol=0.001, cache_size=3000)
	#model = LogisticRegression(penalty='l2', C=1)
	model = RandomForestClassifier(n_estimators=200, criterion="gini", max_depth=None, min_samples_split=10, min_samples_leaf=5, bootstrap=True, oob_score=OOB, n_jobs=10)
	model.fit(X, Y)	
	print model
	print "Preparing test vectors..."
	x = []
	y = []
	i = 0
	for p in testIds[:]:
		if i % 1000 == 0:
			print i, len(testIds)				
		try:
			a = vdb[p[0]]
			b = vdb[p[1]]
		except:
			continue	
		d = []		
		assert len(a) == len(b)
		if FREQS:
			t = 1
			d.append(chi2_kernel([a[0]],[b[0]])[0][0])
		else:
			t = 0
		while t < len(a):				
			if FAST:			
				d.append(fastdtw(a[t], b[t], dist=DIST)[0])
			else:
				d.append(dtw(a[t], b[t], dist=DIST)[0])
			t += 1			
		
		x.append(d)	
		y.append(db[p])
		i += 1
	assert len(x) == len(y)
	print "Predicting..."
	if PROBABILITY:
		yp = model.predict_proba(x)[:,1]
	else:
		yp = model.decision_function(x)
	print "Setting back values..."	
	totyp += list(yp)
	toty += list(y)
	cv_pairs.append(testIds)
	

	U.getScoresSVR(totyp, toty, threshold=None, invert = False, PRINT = True, CURVES = SAVEFIG, SAVEFIG=None)	
	U.aucNth(toty,totyp,1000)
	if STORE_PREDS:
		print "pickling..."
		cPickle.dump((totyp, toty),open("predictions/"+DATASET_BENCHMARK+".valid.predictions.yp.y.cPickle","w"))	
		print "done"	
	homol = [[],[]]#n y
	i = 0
	while i < len(y):		
		homol[y[i]].append(yp[i])
		i+=1
	print len(homol[0])
	print len(homol[1])	
	print "pvalue ", ranksums(homol[0], homol[1])[1]
	plt.violinplot(homol)
	plt.xticks([1,2],["Non homologs", "Homologs"])
	if SAVEFIG:
		plt.savefig("plots/"+DATASET_BENCHMARK+".valid.predDistro.png",dpi=350)
		plt.show()
	print DATASET_BENCHMARK, LEN_FFT,STORE_PREDS, FAST, " OOB: ", OOB	
	return

def vectorizeDCTSeqs(seqdb, ssdb, backbone, side):#ssdb = {UID:[(pred, C, H, E),...])
	db = {}
	for s in seqdb.items(): 
		if RANDOM:
			db[s[0]] = [iDCTquantization(U.getRandomOrthoVect(s[1]),LEN = LEN_FFT)]
		else:					
			db[s[0]] = [U.getFreqs(s[1]), iDCTquantization(U.getOrthoVect(s[1]),LEN = LEN_FFT),\
			iDCTquantization(backbone[s[0]],PLOT = False, LEN = LEN_FFT),\
			iDCTquantization(side[s[0]],PLOT = False, LEN = LEN_FFT),\
			iDCTquantization(U.getSSpred(ssdb[s[0]],1),PLOT = False, LEN = LEN_FFT),\
			iDCTquantization(U.getSSpred(ssdb[s[0]],2),PLOT = False, LEN = LEN_FFT),\
			iDCTquantization(U.getSSpred(ssdb[s[0]],3),PLOT = False, LEN = LEN_FFT),\
			]	
	return db


if __name__ == '__main__':
	main()
