
#######################################################################
###########----Homology Benchmark Description---
#######################################################################

###########----Homologous and Non-homologous pairs------

####---Unique protein multi-domain architectures (UPMDAs) without 16 selected species, and their corresponding protein pairs in the following list of files. Used for Agreement and Disagreement check.

./Homologs_Nonhomologs/alluniqueDarchwdPIDs_pfam_nomax50.txt
./Homologs_Nonhomologs/alluniqueDarchwdPIDs_supfam_nomax50.txt
./Homologs_Nonhomologs/alluniqueDarchwdPIDs_gene3d_nomax50.txt

####---All Homologous pairs were sampled from the above three databases of UPMDA files

./Homologs_Nonhomologs/pfam_hom-pairsallcomb_foreachUPMDA_nomax50.txt
./Homologs_Nonhomologs/supfam_hom-pairsallcomb_foreachUPMDA_nomax50.txt
./Homologs_Nonhomologs/gene3d_hom-pairsallcomb_foreachUPMDA_nomax50.txt


############----Homologous pairs, used for Agreement and Disagreement check-------

./Homologous-pairs_Agree-and-Disagree_with-max50/README_Tables1-9_with-max50.doc
./Homologous-pairs_Agree-and-Disagree_without-max50/README_Tables1-9_without-max50.doc

############----Methods benchmarking pairs-------

####---Homologous benchmark pairs were filtered and sampled from two different sets 1) with Max50 and 2) without Max50. (Removed ambiguous pairs based on their architecture. Kept only either purely Homologous pairs or purely non-homologous pairs)

####---with Max50 filter Homologous pairs ----
./Methods_benchmarking_pairs/pfam_hom-pairs_max50.txt
./Methods_benchmarking_pairs/supfam_hom-pairs_max50.txt
./Methods_benchmarking_pairs/gene3d_hom-pairs_max50.txt

####---without Max50 filter Homologous pairs ----
./Methods_benchmarking_pairs/pfam_hom-pairs_nomax50.txt
./Methods_benchmarking_pairs/supfam_hom-pairs_nomax50.txt
./Methods_benchmarking_pairs/gene3d_hom-pairs_nomax50.txt

####---Non-homologous pairs were sampled at least one random protein from each UPMDA.(Removed ambiguous pairs based on their architecture. Kept only either purely Homologous pairs or purely non-homologous pairs)

####---with Max50 filter Nonhomologous pairs ----
./Methods_benchmarking_pairs/pfam_nonhom-pairs_max50.txt
./Methods_benchmarking_pairs/supfam_nonhom-pairs_max50.txt
./Methods_benchmarking_pairs/gene3d_nonhom-pairs_max50.txt

####---without Max50 filter Nonhomologous pairs ----
./Methods_benchmarking_pairs/pfam_nonhom-pairs_nomax50.txt
./Methods_benchmarking_pairs/supfam_nonhom-pairs_nomax50.txt
./Methods_benchmarking_pairs/gene3d_nonhom-pairs_nomax50.txt


############----Evalues and Bitscores, from all tested methods-------
####---Labeled and sorted raw Evalues and Bitscores of 1-vs-1 mode, from both Max50 and Nomax50 filter sets ----
./Bitscores_and_Evalues/Evalues_max50_1vs1_label/*
./Bitscores_and_Evalues/Evalues_nomax50_1vs1_label/*
./Bitscores_and_Evalues/Bitscores_max50_1vs1_label/*
./Bitscores_and_Evalues/Bitscores_nomax50_1vs1_label/*

####---Ranked Evalues and Bitscores of 1-vs-1 mode, from both Max50 and Nomax50 filter sets ----
./Bitscores_and_Evalues/ROC_plot_Evalues_max50_1vs1_rank/*
./Bitscores_and_Evalues/ROC_plot_Evalues_nomax50_1vs1_rank/*
./Bitscores_and_Evalues/ROC_plot_Bitscores_max50_1vs1_rank/*
./Bitscores_and_Evalues/ROC_plot_Bitscores_nomax50_1vs1_rank/*

############----Scripts for making ROC_plot, from ranked bitscores (we could also use Evalues)------

####---Linear ROC plots for all methods and for three databases----- 
./Bitscores_and_Evalues/ROC_plot_scripts/Fig3_LinearROCplots_Bitscores_max50_1vs1.R

####---Logscale ROC plots for all methods, from ranked evalues----- 
./Bitscores_and_Evalues/ROC_plot_scripts/Fig3_LogscaleROCplots_Bitscores_max50_1vs1.R




