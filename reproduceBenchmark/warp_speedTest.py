#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  homology1.py
#  
#  Copyright 2017 Daniele Raimondi <eddiewrc@arcturus>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import matplotlib.pyplot as plt
from fastdtw._fastdtw import fastdtw
from fastdtw._fastdtw import dtw
from sources.parseBenchmark import parseBench
from sources.parsePSIPRED4 import parsePSIPRED
import sources.utils as U
import numpy as np
import random, cPickle, time
from scipy.stats import ranksums
from sklearn.linear_model import LogisticRegression, RidgeClassifier
from sklearn.svm import SVC		
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics.pairwise import chi2_kernel
from scipy.fftpack import fft, ifft
from scipy.fftpack import dct, idct
FAST = True
PROBABILITY = True
LEN_FFT = 50
STORE_PREDS = True
DATASET_BENCHMARK = "pfam"
SAVEFIG=False
DIST = None
FREQS = True
OOB = True
SIZET = [100,1000,1500,2000,2500,3000,3500,4000,4500,5000,6000,7000,8000,9000,10000]

def main():
	t = []
	i = 0

	for i in SIZET:
		t.append(execute(i))
		i+=1
	print t
	
def mainPlot():
	plt.plot(SIZET, [0.6345009803771973, 3.588747978210449, 4.539480209350586, 5.389642953872681, 6.516814947128296, 6.977652072906494, 7.748491048812866, 8.671624183654785, 9.254233121871948, 10.202035903930664, 11.228199005126953, 12.826558828353882, 14.055132150650024, 15.5154709815979, 16.4145610332489])
	plt.xlabel("Dataset size (number of protein pairs)")
	plt.ylabel("Seconds")
	plt.savefig("testLinearity.png", dpi=400)
	plt.show()

def execute(SIZET):
	#random.seed(100)
	print DATASET_BENCHMARK, LEN_FFT,STORE_PREDS, FAST, " OOB: ", OOB
	db, uids = parseBench("Homology_Benchmark/Methods_benchmarking_pairs/"+DATASET_BENCHMARK+"_hom-pairs_max50.txt","Homology_Benchmark/Methods_benchmarking_pairs/"+DATASET_BENCHMARK+"_nonhom-pairs_max50.txt")
	seqdb = U.readFASTA("Homology_Benchmark/fastas/homologs_"+DATASET_BENCHMARK+".fasta", MIN_LENGTH = 0, MAX_LENGTH=500000) # {id:seq}
	ssdb = parsePSIPRED("data/psipred4_"+DATASET_BENCHMARK+"/")
	backbone = U.readDynaFormat("data/dynaMineResults_"+DATASET_BENCHMARK+"/homologs_backbone.pred")	
	side = U.readDynaFormat("data/dynaMineResults_"+DATASET_BENCHMARK+"/homologs_sidechain.pred")
	vdb = vectorizeDCTSeqs(seqdb, ssdb, backbone, side)
	#cv_pairs = U.randomizePairsPerID(db)#train, test
	#cPickle.dump(cv_pairs, open("separatedPairs.cPickle","w"))
	totyp = []
	toty = []	
	#print "Split train and test"
	trainIds = db.keys()
	
	testIds = db.keys()[:SIZET]
	#testIds = cv_pairs[1]
	#testIds = cv_pairs[1][:100]
	print "Num testids: ",len(testIds)
	#trainIds = cv_pairs[0]
	#assert len(testIds) + len(trainIds) == len(db)	
	print "Preparing vectors..."
	X = []
	Y = []
	i = 0	
	for p in trainIds[:]:
		if i % 1000 == 0:
			print i, len(trainIds)			
		try:
			a = vdb[p[0]]
			b = vdb[p[1]]
		except:
			continue			
		d = []
		assert len(a) == len(b)
		if FREQS:
			t = 1
			d.append(chi2_kernel([a[0]],[b[0]])[0][0])
		else:
			t = 0
		while t < len(a):	
			if FAST:							
				d.append(fastdtw(a[t], b[t], dist=DIST)[0])
			else:
				d.append(dtw(a[t], b[t], dist=DIST)[0])
			t += 1			
		X.append(d)			
		Y.append(db[p])
		i+=1
	assert len(X) == len(Y)
	print "Training..."
	print "Len vectors: ", len(X[0])	
	#model = SVC(C=1.0, kernel='rbf', degree=3, gamma="auto", coef0=0.0, shrinking=True, probability=PROBABILITY, tol=0.001, cache_size=3000)
	#model = LogisticRegression(penalty='l2', C=1)
	model = RandomForestClassifier(n_estimators=200, criterion="gini", max_depth=None, min_samples_split=10, min_samples_leaf=5, bootstrap=True, oob_score=OOB, n_jobs=10)
	model.fit(X, Y)	
	print model
	ids = set()
	for p in testIds:
		ids.add(p[0])
		ids.add(p[1])
	print "Preparing test vectors..."
	t1 = time.time()
	vdb = vectorizeDCTSeqsTest(seqdb, ssdb, backbone, side, ids)
	x = []
	y = []
	i = 0
	for p in testIds[:]:
		if i % 1000 == 0:
			print i, len(testIds)				
		try:
			a = vdb[p[0]]
			b = vdb[p[1]]
		except:
			print "missing"
			continue	
		d = []		
		assert len(a) == len(b)
		if FREQS:
			t = 1
			d.append(chi2_kernel([a[0]],[b[0]])[0][0])
		else:
			t = 0
		while t < len(a):				
			if FAST:			
				d.append(fastdtw(a[t], b[t], dist=DIST)[0])
			else:
				d.append(dtw(a[t], b[t], dist=DIST)[0])
			t += 1				
		x.append(d)	
		y.append(db[p])
		i += 1
	assert len(x) == len(y)
	print "Predicting..."
	if PROBABILITY:
		yp = model.predict_proba(x)[:,1]
	else:
		yp = model.decision_function(x)
	t2 = time.time()
	print "#Elapsed time: %3.3f, size: %d" % (t2 - t1, SIZET)
	return (t2 - t1)
	

def iDCTquantization(v, PLOT = False, LEN = 10): #mean	
	v = np.array(v)		
	if v.ndim > 1:
		v = np.array(v).T		
		yf = dct(v, type=2, norm="ortho")
		trans = idct(yf[:,:LEN], type=2, norm="ortho")			
		i = 0
		tmp = []
		while i < len(trans):
			if not np.any(v[i]):			
				tmp.append(trans[i])
			else:
				tmp.append(scale(trans[i]))			
			i+=1		
		i = 0
		while i < len(tmp):
			if len(tmp) < LEN:
				tmp[i] = tmp[i].tolist() + [0]*(LEN-len(tmp[i]))				
			i += 1		
		tmp = np.array(tmp)
		return tmp.T 	
	else:
		v = scale(np.array(v))			
		yf = dct(v, type=2, norm="ortho")
		trans = idct(yf[:LEN], type=2, norm="ortho")
		tmp = scale(trans)
		if len(tmp) < LEN:
			tmp = list(tmp) + [0]*(LEN-len(tmp))		
		tmp = np.array(tmp)
		return tmp	

def vectorizeDCTSeqsTest(seqdb, ssdb, backbone, side, ids):#ssdb = {UID:[(pred, C, H, E),...])
	db = {}
	for s in seqdb.items(): 
		if s[0] not in ids:
			continue
		#db[s[0]] = [iDCTquantization(U.getRandomOrthoVect(s[1]),LEN = LEN_FFT)]
		db[s[0]] = [U.getFreqs(s[1]), iDCTquantization(U.getOrthoVect(s[1]),LEN = LEN_FFT),\
		iDCTquantization(backbone[s[0]],PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(side[s[0]],PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(U.getSSpred(ssdb[s[0]],1),PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(U.getSSpred(ssdb[s[0]],2),PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(U.getSSpred(ssdb[s[0]],3),PLOT = False, LEN = LEN_FFT),\
		]	
	return db

def vectorizeDCTSeqs(seqdb, ssdb, backbone, side):#ssdb = {UID:[(pred, C, H, E),...])
	db = {}
	for s in seqdb.items(): 
		#db[s[0]] = [iDCTquantization(U.getRandomOrthoVect(s[1]),LEN = LEN_FFT)]
		db[s[0]] = [U.getFreqs(s[1]), iDCTquantization(U.getOrthoVect(s[1]),LEN = LEN_FFT),\
		iDCTquantization(backbone[s[0]],PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(side[s[0]],PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(U.getSSpred(ssdb[s[0]],1),PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(U.getSSpred(ssdb[s[0]],2),PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(U.getSSpred(ssdb[s[0]],3),PLOT = False, LEN = LEN_FFT),\
		]	
	return db

def scale(v, a = 0, b = 1):
	if not np.any(v):
		return v
	M = max(v)
	m = min(v)	
	return (v - m) / float(M - m)	

if __name__ == '__main__':
	main()
