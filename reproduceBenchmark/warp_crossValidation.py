#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  homology1.py
#  
#  Copyright 2017 Daniele Raimondi <eddiewrc@arcturus>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import matplotlib.pyplot as plt
from fastdtw._fastdtw import fastdtw
from fastdtw._fastdtw import dtw
from sources.parseBenchmark import parseBench
from sources.parsePSIPRED4 import parsePSIPRED
import sources.utils as U
import numpy as np
import random, cPickle
from scipy.stats import ranksums
from sklearn.linear_model import LogisticRegression, RidgeClassifier
from sklearn.svm import SVC		
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics.pairwise import chi2_kernel
from sources.iDCTquantization import iDCTquantization
FAST = True
CV_FOLDS = 5
PROBABILITY = True
LEN_FFT = 50
STORE_PREDS = False
DATASET_BENCHMARK = "pfam"
SAVEFIG=False
DIST = None
FREQS = True
OOB = False
# pfam  > Best threshold: 0.497360015801

def main():
	print DATASET_BENCHMARK, CV_FOLDS, LEN_FFT, STORE_PREDS, FAST, "OOB: ",OOB
	db, uids = parseBench("Homology_Benchmark/Methods_benchmarking_pairs/"+DATASET_BENCHMARK+"_hom-pairs_max50.txt","Homology_Benchmark/Methods_benchmarking_pairs/"+DATASET_BENCHMARK+"_nonhom-pairs_max50.txt")
	seqdb = U.readFASTA("Homology_Benchmark/fastas/homologs_"+DATASET_BENCHMARK+".fasta", MIN_LENGTH = 0, MAX_LENGTH=500000) # {id:seq}
	ssdb = parsePSIPRED("data/psipred4_"+DATASET_BENCHMARK+"/")
	backbone = U.readDynaFormat("data/dynaMineResults_"+DATASET_BENCHMARK+"/homologs_backbone.pred")
	side = U.readDynaFormat("data/dynaMineResults_"+DATASET_BENCHMARK+"/homologs_sidechain.pred")
	vdb = vectorizeDCTSeqs(seqdb, ssdb, backbone, side)
	cv_pairs = U.randomizePairs(db, CV_FOLDS)
	fold = 0
	sen = 0.0
	spe = 0.0
	acc = 0.0
	bac = 0.0
	pre = 0.0
	mcc = 0.0
	aucTot = 0.0
	auprcTot = 0.0
	totyp = []
	toty = []
	while fold < CV_FOLDS:
		print "Iteration CV " , fold
		print "Split train and test"
		testIds = cv_pairs.pop(0)
		trainIds = [item for sublist in cv_pairs for item in sublist]
		assert len(testIds) + len(trainIds) == len(db)	
		print "Preparing vectors..."
		X = []
		Y = []
		i = 0	
		for p in trainIds:
			if i % 1000 == 0:
				print i, len(trainIds)			
			try:
				a = vdb[p[0]]
				b = vdb[p[1]]
			except:
				continue			
			d = []			
			assert len(a) == len(b)
			if FREQS:
				t = 1
				d.append(chi2_kernel([a[0]],[b[0]])[0][0])
			else:
				t = 0			
			while t < len(a):	
				if FAST:			
					d.append(fastdtw(a[t], b[t], dist=DIST)[0])
				else:
					d.append(dtw(a[t], b[t], dist=DIST)[0])
				t += 1			
			X.append(d)			
			Y.append(db[p])
			i+=1
		assert len(X) == len(Y)
		print "Training..."
		print "Len vectors: ", len(X[0])	
		#model = SVC(C=1.0, kernel='rbf', degree=3, gamma="auto", coef0=0.0, shrinking=True, probability=PROBABILITY, tol=0.001, cache_size=3000)
		#model = LogisticRegression(penalty='l2', C=1)
		model = RandomForestClassifier(n_estimators=200, criterion="gini", max_depth=None, min_samples_split=10, min_samples_leaf=5, bootstrap=True, oob_score=OOB, n_jobs=10)
		model.fit(X, Y)	
		print model
		print "Preparing test vectors..."
		x = []
		y = []
		i = 0
		for p in testIds[:]:
			if i % 1000 == 0:
				print i, len(testIds)				
			try:
				a = vdb[p[0]]
				b = vdb[p[1]]
			except:
				continue	
			d = []			
			assert len(a) == len(b)
			if FREQS:
				t = 1
				d.append(chi2_kernel([a[0]],[b[0]])[0][0])
			else:
				t = 0	
			while t < len(a):				
				if FAST:			
					d.append(fastdtw(a[t], b[t], dist=DIST)[0])
				else:
					d.append(dtw(a[t], b[t], dist=DIST)[0])
				t += 1			
			x.append(d)			
			y.append(db[p])
			i += 1
		assert len(x) == len(y)
		print "Predicting..."
		if PROBABILITY:
			yp = model.predict_proba(x)[:,1]
		else:
			yp = model.decision_function(x)
		print "Setting back values..."	
		totyp += list(yp)
		toty += list(y)
		cv_pairs.append(testIds)
		fold += 1
		senT, speT, accT, bacT, preT, mccT, aucScore, auprc = U.getScoresSVR(yp, y, threshold=None, invert = False, PRINT = True, CURVES = False)			
		sen += senT
		spe += speT
		acc += accT
		bac += bacT		
		pre += preT
		mcc += mccT	
		aucTot += aucScore
		auprcTot += auprc
	print "-------------FINAL PREDS--------------------"
	print "\nSen = %3.3f" % (sen /float(CV_FOLDS))
	print "Spe = %3.3f" %  (spe/float(CV_FOLDS))
	print "Acc = %3.3f " % (acc/float(CV_FOLDS))
	print "Bac = %3.3f" %  (bac/float(CV_FOLDS))	
	print "Pre = %3.3f" %  (pre/float(CV_FOLDS))
	print "MCC = %3.3f" % (mcc/float(CV_FOLDS))
	print "AUC = %3.3f" % (aucTot/float(CV_FOLDS))
	print "AUCPRC = %3.3f" % (auprcTot/float(CV_FOLDS))
	print "--------------------------------------------"	
	U.getScoresSVR(totyp, toty, threshold=None, invert = False, PRINT = True, CURVES = SAVEFIG, SAVEFIG=None)	
	U.aucNth(toty,totyp,1000)
	if STORE_PREDS:
		print "pickling..."
		cPickle.dump((totyp, toty),open("predictions/"+DATASET_BENCHMARK+".folds"+str(CV_FOLDS)+".LEN"+str(LEN_FFT)+".predictions.yp.y.cPickle","w"))	
		print "done"	
	homol = [[],[]]#n y
	i = 0
	while i < len(y):		
		homol[y[i]].append(yp[i])
		i+=1
	print len(homol[0])
	print len(homol[1])	
	print "pvalue ", ranksums(homol[0], homol[1])[1]
	plt.violinplot(homol)
	plt.xticks([1,2],["Non homologs", "Homologs"])
	if SAVEFIG:
		plt.savefig("plots/"+DATASET_BENCHMARK+".folds"+str(CV_FOLDS)+".LEN"+str(LEN_FFT)+".predDistro.png",dpi=350)
		plt.show()
	print DATASET_BENCHMARK, CV_FOLDS, LEN_FFT, STORE_PREDS, FAST, "OOB: ",OOB
	return

def vectorizeDCTSeqs(seqdb, ssdb, backbone, side):#ssdb = {UID:[(pred, C, H, E),...])
	db = {}
	for s in seqdb.items(): 		
		#db[s[0]] = [iDCTquantization(U.getRandomOrthoVect(s[1]),LEN = LEN_FFT)]		
		
		db[s[0]] = [U.getFreqs(s[1]), iDCTquantization(U.getOrthoVect(s[1]),LEN = LEN_FFT),\
		iDCTquantization(backbone[s[0]],PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(side[s[0]],PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(U.getSSpred(ssdb[s[0]],1),PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(U.getSSpred(ssdb[s[0]],2),PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(U.getSSpred(ssdb[s[0]],3),PLOT = False, LEN = LEN_FFT),\
		]
	return db


if __name__ == '__main__':
	main()
