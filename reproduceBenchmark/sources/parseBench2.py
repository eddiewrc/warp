#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  parseBenchmark.py
#  
#  Copyright 2017 Daniele Raimondi <eddiewrc@arcturus>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import random, os

from parsePSIPRED4 import readPSIPRED4preds

def readDynaFormat(f, PRINT = False):
	db = {}
	print "Reading ",f
	ifp = open(f)
	lines = ifp.readlines()
	ifp.close()
	tmp = []
	name = ""
	
	if not "*" in lines[0]:
		l = 1
	else:
		l = 0
	while l < len(lines):
		if lines[l][0] == "*":
			#print lines[l+4]
			if lines[l+4].count("*") == 1:
				name = lines[l+4].strip().split()[-1]
			elif lines[l+4].count("*") == 2:
				name = lines[l+4].strip().split()[-2]
			else:
				raise Exception("Wrong dynamine format")
			if "earlyFoldProb" in f and len(name) < 7:
				name += "_"
					
			#print "name",name
			if len(name) > 7:
				name = name.replace("_",".")
			#if "d1tdj_2" in lines[l+4]:
			#	print name
			#	raw_input()
			#print name
			#raw_input()
			#print name
			l+=11
		assert lines[l] != "*"
		while l < len(lines) and lines[l][0] != "*":
			if lines[l][0] == " ":
				l+=1
				continue
			#print lines[l][0]
			tmp.append(float(lines[l][3:].strip()))			
			l+=1		
		db[name] = tmp
		#db[name.replace("_",".")] = tmp
		tmp = []
		
		assert "" not in db.keys()
		
	if PRINT == True:
		for p in db.items():
			name = p[0]
			tmp = p[1]
			DIR = "ppIIDir/"
			os.system("mkdir  -p "+DIR)
			plt.plot(tmp)
			plt.title(name)
			plt.savefig(DIR+name+".png",dpi=300)
			plt.clf()
	return db			

def parseBench2(fhom):
	db = {}
	ifp = open(fhom)
	uids = set()
	lines = ifp.readlines()
	l = 0
	while l < len(lines):
		if "Family" in lines[l]:
			name = lines[l].strip().split()[1].split(")")[0]+")"
			#print name
			postext = lines[l+1]
			#print postext
			l+=2
			positives = []
			while not "Negative dataset" in lines[l]:
				tmp = lines[l].strip().split()
				#print tmp				
				positives += tmp
				l+=1	
			#print positives
			#raw_input()
			#print "Found %d positives" % len(positives)			
			negtext = lines[l]
			#print negtext	
			l += 1
			negatives = []
			while l < len(lines) and not "Family" in lines[l]:
				tmp = lines[l].strip().split()				
				negatives += tmp
				l+=1
			#print "Found %d negatives" % len(negatives)
			assert len(positives) == int(postext.split()[3])
			assert len(negatives) == int(negtext.split()[3])
			db[name] = [positives, negatives]
	return db # {familyName:[[positives...], [negatives...]]}

def randomizePairs2(db, PERCNEG=1):
	folds = {}
	fc = 0
	for fam in db.items():
		fc += 1
		#print fam[0]
		folds[fam[0]] = []	#(prot1, prot2, label)
		pos = extractRandom(fam[1][0],len(fam[1][0]))
		tmp = []
		for i in pos:
			tmp.append((i,1))
		folds[fam[0]] += tmp
		neg = extractRandomNeg(fam[1][0], fam[1][1],len(fam[1][1]))
		tmp = []
		for i in neg[:int(round(len(neg)*PERCNEG))]:
			tmp.append((i,0))
		folds[fam[0]] += tmp
		assert len(folds[fam[0]]) == len(pos) + int(round(len(neg)*PERCNEG))
	print "Read %d families" % fc
	return folds #{fam:[((prot1, prot2), label), ...]}

def randomizePairs(db, PERCNEG=1):
	folds = {}
	fc = 0
	for fam in db.items():
		fc += 1
		#print fam[0]
		folds[fam[0]] = []	#(prot1, prot2, label)
		pos = extractRandom(fam[1][0],len(fam[1][0]))
		tmp = []
		for i in pos:
			tmp.append((i,1))
		folds[fam[0]] += tmp
		neg = extractRandom(fam[1][1],len(fam[1][1]))
		tmp = []
		for i in neg[:int(round(len(neg)*PERCNEG))]:
			tmp.append((i,0))
		folds[fam[0]] += tmp
		assert len(folds[fam[0]]) == len(pos) + int(round(len(neg)*PERCNEG))
	print "Read %d families" % fc
	return folds #{fam:[((prot1, prot2), label), ...]}

def extractRandomNeg(posList, negList, num):
	#print protList
	db = set()
	for i in negList:
		tmp = posList[random.randrange(0, len(posList))]
		while tmp == i or tuple(sorted([i,tmp])) in db :			
			tmp = protList[random.randrange(0, len(negList))]
		db.add(tuple(sorted([i,tmp])))
	assert len(db) == num
	return list(db)
	
def extractRandom(protList, num):
	#print protList
	db = set()
	for i in protList:
		tmp = protList[random.randrange(0, len(protList))]
		while tmp == i or tuple(sorted([i,tmp])) in db :			
			tmp = protList[random.randrange(0, len(protList))]
		db.add(tuple(sorted([i,tmp])))
	assert len(db) == num
	return list(db)	

def readFASTAscop(seqFile, MIN_LENGTH = 20, MAX_LENGTH=500):
	ifp = open(seqFile, "r")
	sl = {}
	i = 0
	line = ifp.readline()
	discarded = 0
	
	while len(line) != 0:
		tmp = []
		if line[0] == '>':	
			#sl.append([line.strip().replace("|","").replace(">","")[:5]+"_",""])	#only for spx				
			tmp = [line[1:line.index(" ")],""] #attenzione
			line = ifp.readline()
			while len(line) > 0 and line[0] != '>':
				tmp[1] = tmp[1] + line.strip().upper()
				line = ifp.readline()
			#print len(tmp[1])
			#raw_input()	
			i = i + 1	
			if len(tmp[1]) > MAX_LENGTH or len(tmp[1]) < MIN_LENGTH:
				#print "discard"
				discarded += 1
				continue
			else:				
				sl[tmp[0]] = tmp[1]			
		else:
			raise Exception("Syntax error in the fasta file "+seqFile+"!")	
	print "Found %d sequences, added %d discarded %d" % (i, len(sl), discarded)
	return sl

def parsePSIPRED(folder):
	db = {}
	filelist = os.listdir(folder)
	for f in filelist[:]:
		if not ".ss2" in f:
			continue
		#print folder+f	
		db[f[:-4]] = readPSIPRED4preds(folder+f)
		#raw_input()
	return db

def main(args):
	readDynaFormat("../bench2/dynaMineResults_scop/scop_earlyFoldProb.pred")["d1a1w__"]
	
	seqs =  readFASTAscop("../bench2/scop.1.53.1e-25.fasta")
	#db = parseBench2("../bench2/testing.txt")
	db = parseBench2("../bench2/training.txt")
	f = randomizePairs(db, PERCNEG=0.1)#{fam:[((prot1, prot2), label), ...]}	
	homolLen = [[],[]]#non homol, homol
	for f in f.items():
		for p in f[1]:
			print p
			
			try:
				homolLen[p[1]].append(abs(len(seqs[p[0][0]])-len(seqs[p[0][1]])))
				#if p[1] == 1:
				#	raw_input()
			except:
				pass
	from matplotlib import pyplot as plt
	plt.violinplot(homolLen)
	plt.xticks([1,2],["Non homologs", "Homologs"])
	plt.show()
	return 0

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
