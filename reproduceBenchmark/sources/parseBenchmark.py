#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  parseBenchmark.py
#  
#  Copyright 2017 Daniele Raimondi <eddiewrc@arcturus>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
def parseBench(fhom, fnonhom):
	db = {}
	ifp = open(fhom)
	uids = set()
	lines = ifp.readlines()
	for l in lines:
		tmp = l.strip().split("@")[1].split("_")
		db[tuple(sorted(tmp))] = 1
		uids.add(tmp[0])
		uids.add(tmp[1])
	ifp.close()
	pos = len(db)
	print "Found %d positives" % pos
	ifp = open(fnonhom)
	lines = ifp.readlines()
	for l in lines:
		tmp1 = l.strip().split("@")[0].split("_")[1]
		tmp2 = l.strip().split("@")[1].split("_")[1]
		uids.add(tmp1)
		uids.add(tmp2)
		#print tmp1,tmp2
		db[tuple(sorted([tmp1,tmp2]))] = 0
	print "Found %d negatives" % (len(db) - pos)
	return db, uids

def main(args):
	DS = "supfam"
	db, uids = parseBench("../Homology_Benchmark/Methods_benchmarking_pairs/"+DS+"_hom-pairs_max50.txt","../Homology_Benchmark/Methods_benchmarking_pairs/"+DS+"_nonhom-pairs_max50.txt")

	for i in uids:
		print i
	return 0

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
