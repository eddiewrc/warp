#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  parseBenchmark.py
#  
#  Copyright 2017 Daniele Raimondi <eddiewrc@arcturus>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import os
SS = {"H":[0,1,0],"E":[0,0,1],"C":[1,0,0]}

def parsePSIPRED(folder):
	db = {}
	filelist = os.listdir(folder)
	for f in filelist[:]:
		if not ".ss2" in f:
			continue
		#print folder+f	
		db[f[:f.index(".")]] = readPSIPRED4preds(folder+f)
		#raw_input()
	return db

def readPSIPRED4preds(f):
	ifp = open(f)
	lines = ifp.readlines()
	lines.pop(0)
	lines.pop(0)
	preds = []
	count = 1
	for l in lines:		
		tmp = l.split()
		assert int(tmp[0]) == count
		count += 1
		pred = SS[tmp[2]]
		C = float(tmp[3])
		H = float(tmp[4])
		E = float(tmp[5])
		preds.append((pred, C, H, E))
	return preds	
		


def main():
	print readPSIPRED4preds("../bench2/psipred4_scop153/d1nkd__.ss2")
	print parsePSIPRED("../bench2/psipred4_scop153/")["d1nkd__"]

if __name__ == '__main__':
	main()
