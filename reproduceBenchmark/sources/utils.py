#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it, daniele.raimondi@vub.ac.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
import random
verbosity = 1
amino_acid_codes = [  "A",  "C",  "D",  "E",  "F",  "G",  "H",  "I",  "K",  "L",  "M",  "N",  "P",  "Q",  "R",  "S",  "T",  "V",  "W",  "Y"  ]

def randomizePairsPerID(data):
	print " >>> per ID randomization"
	import random
	l = sorted(data.items(), key=lambda x: x[0])	
	random.shuffle(l)
	train = []
	test = []
	trainSet = set()
	testSet = set()
	uidsTrain = set()
	uidsTest = set()
	i = 0
	while i < len(l):
		if i % 500 == 0:
			print i
		tmp1 = searchSuitable(l, uidsTest, trainSet, None)
		if tmp1 == None:
			print "finished train"
			break
		train.append(tmp1[0])
		trainSet.add(tmp1[0])
		
		uidsTrain.add(tmp1[0][0])
		uidsTrain.add(tmp1[0][1])
		tmp = searchSuitable(l, uidsTrain, testSet, tmp1[1])
		if tmp == None:
			print "finished test"
			break
		
		test.append(tmp[0])
		testSet.add(tmp[0])
		uidsTest.add(tmp[0][0])
		uidsTest.add(tmp[0][1])
		i+=1
	print len(train), len(test)
	assert testSet == set(test)
	assert trainSet == set(train)
	for i in test:
		assert i not in trainSet
	return [train, test]

def searchSuitable(l, uids, ds, label):
	if label != None:
		i = 0
		while i < len(l):
			if l[i][0] not in ds and (l[i][0][0] not in uids and l[i][0][1] not in uids) and l[i][1] == label:
				return l[i]
			i+=1
		return None
	else:
		i = 0
		while i < len(l):
			if l[i][0] not in ds and (l[i][0][0] not in uids and l[i][0][1] not in uids):
				return l[i]
			i+=1
		return None


def getRandomOrthoVect(seq):
	aleph = ["A","R", "N","D","C","Q","E","G","H","I","L","K","M","F","P","S","T","W","Y","V"] 
	r = []
	for s in seq: 	
		tmp = [0]*20
		tmp[random.randint(0,19)] = 1	
		r.append(tmp)	
	#for i in r:
	#	assert len(i) == 20		
	return r

def getOrthoVect(seq):
	aleph = ["A","R", "N","D","C","Q","E","G","H","I","L","K","M","F","P","S","T","W","Y","V"] 
	r = []
	for s in seq: 	
		tmp = [0]*20
		try:
			tmp[aleph.index(s)] = 1	
		except:
			pass
		r.append(tmp)	
	#for i in r:
	#	assert len(i) == 20		
	return r


def getSSpred(ss, v):
	l = []
	for i in ss:
		l.append(i[v])
	return l	

def getFreqs(seq):
	f = []
	for aa in amino_acid_codes:
		f.append(seq.count(aa)/float(len(seq)))
	assert max(f) < 1
	assert len(f) == 20
	return f
	
def readFASTA(seqFile, MIN_LENGTH = 20, MAX_LENGTH=500):
	ifp = open(seqFile, "r")
	sl = {}
	i = 0
	line = ifp.readline()
	discarded = 0
	
	while len(line) != 0:
		tmp = []
		if line[0] == '>':	
			#sl.append([line.strip().replace("|","").replace(">","")[:5]+"_",""])	#only for spx				
			tmp = [line[line.index("|")+1:line.index("|",6)],""] #attenzione
			line = ifp.readline()
			while len(line) > 0 and line[0] != '>':
				tmp[1] = tmp[1] + line.strip()
				line = ifp.readline()
			#print len(tmp[1])
			#raw_input()	
			i = i + 1	
			if len(tmp[1]) > MAX_LENGTH or len(tmp[1]) < MIN_LENGTH:
				#print "discard"
				discarded += 1
				continue
			else:				
				sl[tmp[0]] = tmp[1]			
		else:
			raise Exception("Syntax error in the fasta file "+seqFile+"!")	
	print "Found %d sequences, added %d discarded %d" % (i, len(sl), discarded)
	return sl

def retrieveUID(l):
	uidList = []
	for i in l:
		ifp = open(i, "r")
		lines = ifp.readlines()
		for j in lines:
			uidList.append(j.strip())		
	if verbosity >= 2:
		print "Num of uid retrieved: " + str(len(uidList))
	return uidList	
	
def getSeqIDsimilarity(s1,s2): #change it with your favourite similarity measure!
	score = 0
	i = 0 
	while i < min(len(s1), len(s2)):
		if s1[i] == s2[i] and s1[i] != "-":
			score += 1
		i += 1
	return score/float(len(s2)-s2.count("-")) 
	#return score/float(len(s1))
	#return score/float(len(s1))		


def specialUnion(blacklist, new, old):
	for i in new:
		if i not in blacklist:
			if type(old) == list:
				old.append(i)
			else:
				old.add(i)
	return old
	

def getAllOccurrences(target, db):
	l = set()
	pairs = []
	for i in db.keys():
		if target == i[0] or target == i[1]:
			l.add(i[0])
			l.add(i[1])
			pairs.append(i)
	return pairs, l	

def randomizePairs(data, CV_FOLDS):
	import random
	l = data.keys()
	for i in range(1,5,1):
		random.shuffle(l)
	size = len(l) / CV_FOLDS
	folds = []
	i = 0
	tot = 0
	while i < CV_FOLDS:
		if i == CV_FOLDS -1:
			tmp = l[i*size:]
		else:
			tmp = l[i*size:size*(i+1)]
		folds.append(tmp)
		#print len(tmp)
		tot += len(tmp)
		i+=1
	assert tot == len(l)
	return folds

	
def getScoresSVR(pred, real, threshold=None, invert = False, PRINT = False, CURVES = False, SAVEFIG=None):
	import math
	if len(pred) != len(real):
		raise Exception("ERROR: input vectors have differente len!")
	if PRINT:
		print "Computing scores for %d predictions" % len(pred)		
	from sklearn.metrics import roc_curve, auc, average_precision_score, precision_recall_curve
	
	if CURVES or SAVEFIG != None:
		import matplotlib.pyplot as plt		
		precision, recall, thresholds = precision_recall_curve(real, pred)
		#plt.plot(recall, precision)
		#plt.show()
		fpr, tpr, _ = roc_curve(real, pred)		
		fig, (ax1, ax2) = plt.subplots(figsize=[10.0, 5], ncols=2)
		ax1.set_ylabel("Precision")
		ax1.set_xlabel("Recall")
		ax1.set_title("PR curve")
		ax1.set_xlim(0,1)
		ax1.set_ylim(0,1)
		ax1.plot(recall, precision)
		ax1.grid()
		ax2.plot(fpr, tpr)
		ax2.set_ylim(0,1)
		ax2.set_xlim(0,1)
		ax2.plot([0,1],[0,1],"--",c="grey",)
		ax2.set_xlabel("FPR")
		ax2.set_ylabel("TPR")
		ax2.set_title("ROC curve")
		ax2.grid()
		if SAVEFIG != None:
			plt.savefig(SAVEFIG, dpi=400)
		plt.show()
		plt.clf()
		
	fpr, tpr, thresholds = roc_curve(real, pred)
	auprc = average_precision_score(real, pred)
	aucScore = auc(fpr, tpr)		
	i = 0
	r = []
	while i < len(fpr):
		r.append((fpr[i], tpr[i], thresholds[i]))
		i+=1	
	ts = sorted(r, key=lambda x:(1.0-x[0]+x[1]), reverse=True)[:3]	
	#if PRINT:
	#	print ts
	if threshold == None:
		if PRINT:
			print " > Best threshold: " + str(ts[0][2])
		threshold = ts[0][2]
	i = 0
	confusionMatrix = {}
	confusionMatrix["TP"] = confusionMatrix.get("TP", 0)
	confusionMatrix["FP"] = confusionMatrix.get("FP", 0)
	confusionMatrix["FN"] = confusionMatrix.get("FN", 0)
	confusionMatrix["TN"] = confusionMatrix.get("TN", 0)
	if invert == True:
		while i < len(real):
			if float(pred[i])>=threshold and (real[i]==0):
				confusionMatrix["TN"] = confusionMatrix.get("TN", 0) + 1
			if float(pred[i])>=threshold and real[i]==1:
				confusionMatrix["FN"] = confusionMatrix.get("FN", 0) + 1
			if float(pred[i])<=threshold and real[i]==1:
				confusionMatrix["TP"] = confusionMatrix.get("TP", 0) + 1
			if float(pred[i])<=threshold and real[i]==0:
				confusionMatrix["FP"] = confusionMatrix.get("FP", 0) + 1
			i += 1
	else:
		while i < len(real):
			if float(pred[i])<=threshold and (real[i]==0):
				confusionMatrix["TN"] = confusionMatrix.get("TN", 0) + 1
			if float(pred[i])<=threshold and real[i]==1:
				confusionMatrix["FN"] = confusionMatrix.get("FN", 0) + 1
			if float(pred[i])>=threshold and real[i]==1:
				confusionMatrix["TP"] = confusionMatrix.get("TP", 0) + 1
			if float(pred[i])>=threshold and real[i]==0:
				confusionMatrix["FP"] = confusionMatrix.get("FP", 0) + 1
			i += 1
	#print "--------------------------------------------"
	#print confusionMatrix["TN"],confusionMatrix["FN"],confusionMatrix["TP"],confusionMatrix["FP"]
	if PRINT:
		print "      | DEL         | NEUT             |"
		print "DEL   | TP: %d   | FP: %d  |" % (confusionMatrix["TP"], confusionMatrix["FP"] )
		print "NEUT  | FN: %d   | TN: %d  |" % (confusionMatrix["FN"], confusionMatrix["TN"])	
	
	sen = (confusionMatrix["TP"]/max(0.00001,float((confusionMatrix["TP"] + confusionMatrix["FN"]))))
	spe = (confusionMatrix["TN"]/max(0.00001,float((confusionMatrix["TN"] + confusionMatrix["FP"]))))
	acc =  (confusionMatrix["TP"] + confusionMatrix["TN"])/max(0.00001,float((sum(confusionMatrix.values()))))
	bac = (0.5*((confusionMatrix["TP"]/max(0.00001,float((confusionMatrix["TP"] + confusionMatrix["FN"])))+(confusionMatrix["TN"]/max(0.00001,float((confusionMatrix["TN"] + confusionMatrix["FP"])))))))
	inf =((confusionMatrix["TP"]/max(0.00001,float((confusionMatrix["TP"] + confusionMatrix["FN"])))+(confusionMatrix["TN"]/max(0.00001,float((confusionMatrix["TN"] + confusionMatrix["FN"])))-1.0)))
	pre =(confusionMatrix["TP"]/max(0.00001,float((confusionMatrix["TP"] + confusionMatrix["FP"]))))
	mcc =	( ((confusionMatrix["TP"] * confusionMatrix["TN"])-(confusionMatrix["FN"] * confusionMatrix["FP"])) / max(0.00001,math.sqrt((confusionMatrix["TP"]+confusionMatrix["FP"])*(confusionMatrix["TP"]+confusionMatrix["FN"])*(confusionMatrix["TN"]+confusionMatrix["FP"])*(confusionMatrix["TN"]+confusionMatrix["FN"]))) )  
	
	if PRINT:
		print "\nSen = %3.3f" % sen
		print "Spe = %3.3f" %  spe
		print "Acc = %3.3f " % acc
		print "Bac = %3.3f" %  bac
		#print "Inf = %3.3f" % inf
		print "Pre = %3.3f" %  pre
		print "MCC = %3.3f" % mcc
		print "#AUC = %3.3f" % aucScore
		print "#AUPRC= %3.3f" % auprc
		print "--------------------------------------------"	
	
	return sen, spe, acc, bac, pre, mcc, aucScore, auprc

def getLprecision(pred, real, bestLnum):
	tmp = []
	assert len(pred) == len(real)
	i = 0
	while i < len(pred):
		tmp.append((pred[i], real[i]))
		i += 1
	tmp = sorted(tmp, key=lambda x: x[0], reverse=True)
	bestL = tmp[:bestLnum]
	assert len(bestL) == bestLnum
	tot = 0
	for i in bestL:
		tot+=i[1]
	print "Precision best %d prots preds: %3.2f" % (len(bestL),float(tot)/float(bestLnum))	
	return float(tot)/float(len(bestL))

def writeFasta(db, name):
	ofp = open(name, "w")
	for i in db.items():
		ofp.write(">"+str(i[0])+"\n"+i[1][0]+"\n")
	ofp.close()

def readDynaFormat(f, PRINT = False):
	db = {}
	print "Reading ",f
	ifp = open(f)
	lines = ifp.readlines()
	ifp.close()
	tmp = []
	name = ""
	
	if not "*" in lines[0]:
		l = 1
	else:
		l = 0
	while l < len(lines):
		if lines[l][0] == "*":
			#print lines[l+4]
			name = lines[l+4][lines[l+4].index("_")+1:lines[l+4].index("_",10)].strip()
			#print name
			#raw_input()
			#print name
			l+=11
		assert lines[l] != "*"
		while l < len(lines) and lines[l][0] != "*":
			if lines[l][0] == " ":
				l+=1
				continue
			#print lines[l][0]
			tmp.append(float(lines[l][3:].strip()))			
			l+=1		
		db[name] = tmp
		#db[name.replace("_",".")] = tmp
		tmp = []
		
		assert "" not in db.keys()
		
	if PRINT == True:
		for p in db.items():
			name = p[0]
			tmp = p[1]
			DIR = "ppIIDir/"
			os.system("mkdir  -p "+DIR)
			plt.plot(tmp)
			plt.title(name)
			plt.savefig(DIR+name+".png",dpi=300)
			plt.clf()
	return db	

def aucNth(y, yp, N):
	assert len(y) == len(yp)
	assert len(y) > 1
	from sklearn.metrics import roc_curve, auc, roc_auc_score 
	fpr, tpr, thresholds = roc_curve(y, yp)
	negatives = y.count(0)
	assert N < negatives
	perc = N / float(negatives)	
	#print perc
	fpr1k = []
	tpr1k = []
	i = 0
	while i < len(fpr):		
		if fpr[i] > perc:
			break
		fpr1k.append(fpr[i])
		tpr1k.append(tpr[i])
		i+=1	
	assert len(fpr1k) > 1
	#print fpr1k, tpr1k
	aucScore = auc(fpr1k, tpr1k) / perc 	
	print ">>AUC%d: %3.3f" %(N,aucScore)
	return aucScore

def main():
	
	print aucNth([0,0,0,0,1,1,0,0,0,0,1,1,1],[5.1,1.5,1.2,1.4,1.1,0.1,9.1,1.9,0.7,0.8,0.4,0.7,0.2],5)
	raw_input()
	DATASET_BENCHMARK = "pfam"
	CV_FOLDS = 5
	from parseBenchmark import parseBench
	db, uids = parseBench("../Homology_Benchmark/Methods_benchmarking_pairs/"+DATASET_BENCHMARK+"_hom-pairs_max50.txt","../Homology_Benchmark/Methods_benchmarking_pairs/"+DATASET_BENCHMARK+"_nonhom-pairs_max50.txt")
	cv_pairs = randomizePairsPerID(db, CV_FOLDS)
	
	#db = readFASTA1("../universe/uni20.fasta", MIN_LENGTH = 20, MAX_LENGTH=500)
	#db = readFASTA("../universe/uni20.fasta", MIN_LENGTH = 20, MAX_LENGTH=500)
	db = readDynaFormat("../dynaMineResults/homologs_backbone.pred")
	print db["A0A087X1C5"]
	print len(db)
	print db.keys()[:5]
	
	

if __name__ == '__main__':
	main()


