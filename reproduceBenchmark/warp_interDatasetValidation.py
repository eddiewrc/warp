#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  homology1.py
#  
#  Copyright 2017 Daniele Raimondi <eddiewrc@arcturus>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import sources.parseBench2 as PA
import matplotlib.pyplot as plt
from fastdtw._fastdtw import fastdtw
from fastdtw._fastdtw import dtw
from sources.parseBenchmark import parseBench
from sources.parsePSIPRED4 import parsePSIPRED
import sources.utils as U
import numpy as np
import random, cPickle, sys
from scipy.stats import ranksums
from sklearn.linear_model import LogisticRegression, RidgeClassifier
from sklearn.svm import SVC		
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics.pairwise import chi2_kernel
from sources.iDCTquantization import iDCTquantization
RANDOM = False
FAST = True
PROBABILITY = True
LEN_FFT = 50
STORE_PREDS = False
TRAINSETS = ["supfam","pfam"]#, "gene3d", "supfam"]
TESTSETS = ["gene3d"]#, "gene3d", "supfam"]
SAVEFIG=False
DIST = None
FREQS = True
FAMILY = True
if RANDOM:
	FREQS = False
OOB = False

def buildVectorsTrain(DATASET_BENCHMARK, LEN_FFT, TESTDB = None):
	random.seed(100)
	print DATASET_BENCHMARK, LEN_FFT,STORE_PREDS, FAST, " OOB: ", OOB
	db, uids = parseBench("Homology_Benchmark/Methods_benchmarking_pairs/"+DATASET_BENCHMARK+"_hom-pairs_max50.txt","Homology_Benchmark/Methods_benchmarking_pairs/"+DATASET_BENCHMARK+"_nonhom-pairs_max50.txt")
	
	if TESTDB != None:
		testdb, testuids = parseBench("Homology_Benchmark/Methods_benchmarking_pairs/"+TESTDB+"_hom-pairs_max50.txt","Homology_Benchmark/Methods_benchmarking_pairs/"+TESTDB+"_nonhom-pairs_max50.txt")
	#print testuids
	#raw_input()	
	seqdb = U.readFASTA("Homology_Benchmark/fastas/homologs_"+DATASET_BENCHMARK+".fasta", MIN_LENGTH = 0, MAX_LENGTH=500000) # {id:seq}
	ssdb = parsePSIPRED("data/psipred4_"+DATASET_BENCHMARK+"/")
	backbone = U.readDynaFormat("data/dynaMineResults_"+DATASET_BENCHMARK+"/homologs_backbone.pred")	
	side = U.readDynaFormat("data/dynaMineResults_"+DATASET_BENCHMARK+"/homologs_sidechain.pred")
	vdb = vectorizeDCTSeqs(seqdb, ssdb, backbone, side)
	trainIds = db.keys()
	
	#assert len(testIds) + len(trainIds) == len(db)	
	print "Preparing vectors..."
	X = []
	Y = []
	i = 0	
	dup = 0
	for p in trainIds[:]:
		if TESTDB != None and (p[0] in testuids or p[1] in testuids): #pairs
			#print "DUPLICATE"
			dup += 1
			continue
		
		'''if TESTDB != None and p in testdb.keys() : #pairs
			#print "DUPLICATE"
			dup += 1
			continue'''
		
		if i % 1000 == 0:
			print i, len(trainIds)			
		try:
			a = vdb[p[0]]
			b = vdb[p[1]]
		except:
			continue			
		d = []
		assert len(a) == len(b)
		if FREQS:
			t = 1
			d.append(chi2_kernel([a[0]],[b[0]])[0][0])
		else:
			t = 0
		while t < len(a):	
			if FAST:							
				d.append(fastdtw(a[t], b[t], dist=DIST)[0])
			else:
				d.append(dtw(a[t], b[t], dist=DIST)[0])
			t += 1			
		X.append(d)			
		Y.append(db[p])
		i+=1
	assert len(X) == len(Y)
	if TESTDB != None:
		print "Removed duplicates = ", dup
	
	print "Len vectors: ", len(X[0])
	print "Num vectors: ", len(X)
	print "Labels: ", sum(Y)/float(len(Y))	
	return X, Y

def main():
	X = []
	Y = []
	for ds in TRAINSETS:
		x, y = buildVectorsTrain(ds, LEN_FFT, TESTSETS[0])
		X += x
		Y += y
	assert len(X) == len(Y)
	print "FINAL X len: " , len(X)	
	#model = SVC(C=1.0, kernel='rbf', degree=3, gamma="auto", coef0=0.0, shrinking=True, probability=PROBABILITY, tol=0.001, cache_size=3000)
	#model = LogisticRegression(penalty='l2', C=1)
	model = RandomForestClassifier(n_estimators=200, criterion="gini", max_depth=None, min_samples_split=10, min_samples_leaf=5, bootstrap=True, oob_score=OOB, n_jobs=10)
	model.fit(X, Y)	
	print model
	
	x = []
	y = []
	for ds in TESTSETS:
		x1, y1 = buildVectorsTrain(ds, LEN_FFT)
		x += x1
		y += y1	
	
	assert len(x) == len(y)
	print "Predicting...", len(y)
	if PROBABILITY:
		yp = model.predict_proba(x)[:,1]
	else:
		yp = model.decision_function(x)
	print "Setting back values..."	
	
	U.getScoresSVR(yp, y, threshold=0.5, invert = False, PRINT = True, CURVES = True)	
	U.aucNth(y,yp, 1000)	
	print "Trainset: ", TRAINSETS
	print "Testset: ", TESTSETS
	sys.stdout.flush()
	return

def vectorizeDCTSeqs(seqdb, ssdb, backbone, side):#ssdb = {UID:[(pred, C, H, E),...])
	db = {}
	for s in seqdb.items(): 
		#db[s[0]] = [iDCTquantization(getRandomOrthoVect(s[1]),LEN = LEN_FFT)]
		
		db[s[0]] = [U.getFreqs(s[1]), iDCTquantization(U.getOrthoVect(s[1]),LEN = LEN_FFT),\
		iDCTquantization(backbone[s[0]],PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(side[s[0]],PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(U.getSSpred(ssdb[s[0]],1),PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(U.getSSpred(ssdb[s[0]],2),PLOT = False, LEN = LEN_FFT),\
		iDCTquantization(U.getSSpred(ssdb[s[0]],3),PLOT = False, LEN = LEN_FFT),\
		]
	return db
if __name__ == '__main__':
	main()
